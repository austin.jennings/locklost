import argparse

import gpstime


class GPSTimeParseAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=False):
        try:
            gps = gpstime.parse(values).gps()
        except TypeError:
            gps = [gpstime.parse(value).gps() for value in values]
        except gpstime.GPSTimeException:
            parser.error("Could not parse date/time string '{}'".format(values))
        setattr(namespace, self.dest, gps)
